/*
* cmm.cpp
*
* Created on: Sep 6, 2011
* Author: root
*
* This is the implementation file for CCMM - communication module
*/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <termios.h>
#include <pthread.h>
#include <ifaddrs.h>

#include "common.h"
#include "task.h"
#include "cam.h"
#include "img.h"
#include "cmmSerialGumstix.h"

CCMMSERGUMSTIX::CCMMSERGUMSTIX()
{
    m_nsCMI = 0;
    memset(&m_dataFromGumstix, 0, sizeof(DATASTRUCT_GUMSTIX2VISION));
    memset(&m_dataToGumstix, 0, sizeof(DATASTRUCT_VISION2GUMSTIX));
}

CCMMSERGUMSTIX *CCMMSERGUMSTIX::GetCmmSerialGumstix()
{
    static CCMMSERGUMSTIX *_cmmSerialGumstix = 0;
    if(_cmmSerialGumstix == 0)
    {
        _cmmSerialGumstix= new CCMMSERGUMSTIX();
    }
    return _cmmSerialGumstix;
}

BOOL CCMMSERGUMSTIX::InitTask()
{
    CTask::InitTask();

    m_nsCMI = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);

    if(m_nsCMI == -1)
    {
        printf("[CCMMSERGUMSTIX] open fail\n");
        return 1;
    }

    termios term;
    tcgetattr(m_nsCMI, &term); //configuration
    bzero(&term, sizeof(termios));

    cfsetispeed(&term, B115200);
    cfsetospeed(&term, B115200);

    term.c_cflag &= ~(CS5|CS6|CS7|HUPCL|PARENB | CSTOPB | CSIZE);
    term.c_cflag |= (CS8|CLOCAL|CREAD);
    term.c_iflag = 0;
    term.c_oflag = 0;


    tcflush(m_nsCMI, TCIOFLUSH);
    tcsetattr(m_nsCMI, TCSANOW, &term);

    usleep(500000);

    m_nBuffer=0;
    printf("[CCMMSERGUMSTIX] start\n");
    return TRUE;
}

void CCMMSERGUMSTIX::ExitTask()
{
    CTask::ExitTask();
    printf("[CCMMSERGUMSTIX] exit\n");
}

int CCMMSERGUMSTIX::Run1()
{
    //printf("[CCMMSERGUMSTIX] Run1\n");
    char buf[300];
    mavlink_message_t message;
    mavlink_mastermind_2_gumstix_t data2gumstix;
    memset(&data2gumstix, 0, sizeof(mavlink_mastermind_2_gumstix_t));

//    printf("sizeof mavlink_mastermind_2_gumstix_t: %d int64_t: %d int8_t: %d float: %d\n", sizeof(mavlink_mastermind_2_gumstix_t), sizeof(int64_t), sizeof(uint8_t), sizeof(float));

    data2gumstix.time_usec = ::GetTime()*1e6;

    for (int i = 0; i < 6; i++)
    {
        data2gumstix.reserved_bool[i] = CIMG::GetIMG()->GetIMGoutput().visionFlag[i];
    }

    for (int i = 0 ; i < 3; i++){
        data2gumstix.position[i] = CIMG::GetIMG()->GetIMGoutput().tvec[i]; //camera frame;
        data2gumstix.reserved_float[i] = CIMG::GetIMG()->GetIMGoutput().poseNED[i]; // used for bucket location
    }

    mavlink_msg_mastermind_2_gumstix_encode(10, 200, &message, &data2gumstix);
    unsigned len = mavlink_msg_to_send_buffer((uint8_t*)buf, &message);

    int nsend = write(m_nsCMI, buf, len);
    if(nsend == -1)
    {
        perror("[CCMMSERGUMSTIX::SendTelegaph] Error: ");
    }

//receive data from Gumstix;

    int nRead = read(m_nsCMI, m_buffer, MAX_CMM_BUFFER);	//get data as many as it can
    if (nRead <= 0)
    {
        return 0;
    }

    mavlink_message_t msg;
    mavlink_status_t  status;
    for (ssize_t i = 0; i < nRead; i++){
        if(mavlink_parse_char(MAVLINK_COMM_1, m_buffer[i], &msg, &status)){
            handle_message(&msg);
        }
    }

}

void CCMMSERGUMSTIX::handle_message(mavlink_message_t *msg){
    switch(msg->msgid){
        case MAVLINK_MSG_ID_gumstix_2_mastermind:
            mavlink_gumstix_2_mastermind_t dataFromGumstix;
            mavlink_msg_gumstix_2_mastermind_decode(msg, &dataFromGumstix);

            m_dataFromGumstix.gumstixTime = dataFromGumstix.time_usec/1e6;
            m_dataFromGumstix.x = dataFromGumstix.lon/1e7;
            m_dataFromGumstix.y = dataFromGumstix.lat/1e7;
            m_dataFromGumstix.z = dataFromGumstix.alt/1e7;

            m_dataFromGumstix.ug = dataFromGumstix.velocity_ned[0];
            m_dataFromGumstix.vg = dataFromGumstix.velocity_ned[1];
            m_dataFromGumstix.wg = dataFromGumstix.velocity_ned[2];

            m_dataFromGumstix.a = dataFromGumstix.eulerAngle[0];
            m_dataFromGumstix.b = dataFromGumstix.eulerAngle[1];
            m_dataFromGumstix.c = dataFromGumstix.eulerAngle[2];

            bool bSetExposure = dataFromGumstix.reserved_bool[0];
            float exposure = dataFromGumstix.reserved_float[0];
            if (bSetExposure){
                CCAM::GetCAM()->SetExposure(exposure);
            }

           // printf("gumstix Time: %.2f %d %f\n", m_dataFromGumstix.gumstixTime, bSetExposure, exposure);
        break;
    }
}